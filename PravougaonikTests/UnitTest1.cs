﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Termin10Primer01;

namespace PravougaonikTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestiranjePovrsine()
        {
            Pravougaonik p1 = new Pravougaonik(5, 6);
            double ocekivanaPovrsina = 30;
            double dobijenaPovrsina = p1.IzracunajPovrsinu();
            Assert.AreEqual(ocekivanaPovrsina, dobijenaPovrsina,
                "Izracunavanje povrsine ne valja!");
        }
        [TestMethod]
        public void TestiranjeObima()
        {
            Pravougaonik p1 = new Pravougaonik(5, 6);
            double ocekivanObim = 22;
            double dobijenObim = p1.IzracunajObim();
            Assert.AreEqual(ocekivanObim, dobijenObim,
                "Izracunavanje obima ne valja!");
        }
        [TestMethod]
        public void TestiranjeKvadrata()
        {
            Pravougaonik p1 = new Pravougaonik(5, 5);
            Assert.IsTrue(p1.DaLiJeKvadrat());

            Pravougaonik p2 = new Pravougaonik(6, 7);
            Assert.IsFalse(p2.DaLiJeKvadrat());
        }
    }
}
